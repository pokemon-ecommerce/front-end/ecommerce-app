import {string, object} from 'yup';

const exprirationDateMessage = 'Not a valid expiration date. Example: MM/YY';
const cardNumberMessage = 'Not a valid card number.';

const checkoutFormScheme = object().shape({
    cardNumber: string()
        .label('Card Number')
        .typeError(cardNumberMessage)
        .max(16, cardNumberMessage)
        .matches(/(\d{16})/, cardNumberMessage)
        .required(),
    dueDate: string()
        .label('Expiration date')
        .typeError(exprirationDateMessage)
        .max(5, exprirationDateMessage)
        .matches(/(\d{2})\/(\d{2})/, exprirationDateMessage)
        .required(),
});

export default checkoutFormScheme;
