import {string, object} from 'yup';

const loginFormScheme = object().shape({
    email: string().email().required('This field is required'),
    password: string().required(),
});

export default loginFormScheme;
