import {string, object} from 'yup';

const searchPoductFormScheme = object().shape({
    name: string().required('This field is required'),
});

export default searchPoductFormScheme;
