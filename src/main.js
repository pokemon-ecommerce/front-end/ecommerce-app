import {createApp} from 'vue';
import VueLazyLoad from 'vue3-lazyload';
import App from './containers/app/app';
import {router} from './router/router';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap';
import 'bootstrap-icons/font/bootstrap-icons.css';
import store from './store/store';
import './assets/styles/index.scss';

createApp(App).use(store).use(VueLazyLoad).use(router).mount('#app');
