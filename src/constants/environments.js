export const env = {
    SUPER_HERO_API_URL: process.env.VUE_APP_SUPER_HERO_API_URL,
    SUPER_HERO_TOKEN_API: process.env.VUE_APP_SUPER_HERO_TOKEN_API,
};
