class Product {
    constructor({name = null, description = null, price = null, image = null,stock=null} = {}) {
        this.name = name;
        this.description = description;
        this.price = price ? price.toFixed(2): 0;
        this.image = image;
        this.stock = stock;
    }
}

export default Product;
