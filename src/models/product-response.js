class ProductResponse {
    constructor({
        totalItems = null,
        currentPage = null,
        totalPages = null,
        products = [],
    } = {}) {
        this.totalItems = totalItems;
        this.currentPage = currentPage;
        this.totalPages = totalPages;
        this.products = products;
    }
}

export default ProductResponse;
