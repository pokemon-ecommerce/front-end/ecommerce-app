class Checkout {
    constructor({order = null, payment = null} = {}) {
        this.order = order;
        this.payment = payment;
    }
}

export default Checkout;
