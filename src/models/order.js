class Order {
    constructor({userId = null, total = null, details = []} = {}) {
        this.userId = userId;
        this.total = total;
        this.details = details;
    }
}

export default Order;
