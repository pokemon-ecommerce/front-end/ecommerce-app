class TokenResponse {
    constructor({
        accessToken = null,
        tokenType = null,
        userId = null,
        email = null,
        name = null,
    } = {}) {
        this.token = accessToken && tokenType ? `${tokenType} ${accessToken}` : null;
        this.userId = userId;
        this.email = email;
        this.name = name;
    }
}

export default TokenResponse;
