export const pokemonResponse = {
    totalItems: 20,
    totalPages: 5,
    currentPage: 1,
    products: [
        {
            id: 1,
            name: 'Charizard',
            description: 'Charizard Card',
            price: 25.0,
            image: 'https://assets.pokemon.com/assets/cms2-es-es/img/cards/web/SWSH3/SWSH3_ES_19.png',
            stock: 20
        },
        {
            id: 2,
            name: 'Pikachu',
            description: 'Pikachu Card',
            price: 20.0,
            image: 'https://assets.pokemon.com/assets/cms2-es-es/img/cards/web/SMP/SMP_ES_SM86.png',
            stock: 20
        },
        {
            id: 3,
            name: 'Mew',
            description: 'Once during your turn (before your attack), if this Pokémon is your Active Pokémon, you may have your opponent reveal his or her hand.',
            price: 20.00,
            image: 'https://assets.pokemon.com/assets/cms2/img/cards/web/SWSH8/SWSH8_EN_114.png',
            stock: 20
        },
        {
            id: 4,
            name: 'Mewtwo',
            description: 'Mewtwo Card',
            price: 20.0,
            image: 'https://assets.pokemon.com/assets/cms2/img/cards/web/SM11/SM11_EN_222.png',
            stock: 20
        },
        {
            id: 5,
            name: 'Meltan',
            description: 'Meltan Card',
            price: 20.0,
            image: 'https://assets.pokemon.com/assets/cms2/img/cards/web/SWSH3/SWSH3_EN_129.png',
            stock: 20
        },
        {
            id: 6,
            name: 'Articuno',
            description: 'Articuno Card',
            price: 20.0,
            image: 'https://assets.pokemon.com/assets/cms2/img/cards/web/SWSH6/SWSH6_EN_58.png',
            stock: 20
        },
    ],
};
