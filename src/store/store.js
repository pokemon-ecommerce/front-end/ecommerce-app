import {createStore, createLogger} from 'vuex';
import {makeServer} from '../server';
import user from './modules/user/state';
import product from './modules/products/state';

const debug = process.env.NODE_ENV !== 'production';

if (debug) {
    makeServer();
}

export default createStore({
    modules: {
        user,
        product
    },
    strict: debug,
    plugins: debug ? [createLogger()] : [],
});
