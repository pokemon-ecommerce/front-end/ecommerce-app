import {ADD_PRODUCT, CLEAN_CART, REMOVE_PRODUCT, SET_PURSCHASE_ID} from './product-types';

const mutations = {
    [ADD_PRODUCT](state, payload) {
        state.data = payload;
    },
    [REMOVE_PRODUCT](state, payload) {
        state.data = payload;
    },
    [CLEAN_CART](state) {
        state.data = [];
    },
    [SET_PURSCHASE_ID](state, payload) {
        state.purchaseId = payload;
    },
};

export default mutations;
