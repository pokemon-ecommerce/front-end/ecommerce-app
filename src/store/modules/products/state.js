import actions from './actions';
import mutations from './mutations';
import getters from './getters';

const state = {
    data: [],
    loading: false,
    error: null,
    purchaseId: null,
};

export default {
    namespaced: true,
    getters,
    state,
    actions,
    mutations,
};
