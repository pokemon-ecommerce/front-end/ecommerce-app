import {ADD_PRODUCT, CLEAN_CART, REMOVE_PRODUCT, SET_PURSCHASE_ID} from './product-types';

const actions = {
    addProduct({commit, state}, payload) {
        const products = [...state.data];
        const index = products.findIndex((product) => product.id === payload.id);
        console.log(`index`, index);
        if (index >= 0) {
            products[index] = {
                ...products[index],
                amount: products[index].amount + payload.amount,
            };
            commit(ADD_PRODUCT, [...products]);
            return;
        }
        commit(ADD_PRODUCT, [...products, payload]);
    },
    removeProduct({commit, state}, payload) {
        const products = [...state.data];
        const index = products.findIndex((product) => product.id === payload.id);
        products.splice(index, 1);
        commit(REMOVE_PRODUCT, products);
    },
    purchase({commit}, payload) {
        commit(CLEAN_CART);
        commit(SET_PURSCHASE_ID, payload);
    },
};

export default actions;
