const getters = {
    getProducts: (state) => state.data || [],
    total: (state) => {
        return state.data
            .map((item) => item.price * item.amount)
            .reduce((a, b) => a + b, 0);
    },
    totalProducts: (state) => {
        const products = state.data || [];
        return products.length || 0;
    },
    getPurchaseId: (state) => state.purchaseId,
};

export default getters;
