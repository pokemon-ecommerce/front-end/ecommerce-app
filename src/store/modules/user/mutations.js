import {TokenResponse} from '@/models';
import {LOGOUT, SET_USER, SET_USER_ERROR, SET_USER_LOADING} from './user-types';

const mutations = {
    [SET_USER](state, payload) {
        state.data = {
            ...state.data,
            ...payload,
        };
    },
    [LOGOUT](state) {
        state.data = new TokenResponse();
    },
    [SET_USER_ERROR](state, payload) {
        state.error = payload;
    },
    [SET_USER_LOADING](state, payload) {
        state.loading = payload;
    },
};

export default mutations;
