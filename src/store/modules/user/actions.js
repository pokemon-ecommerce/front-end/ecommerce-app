import {UserApiProvider} from '@/providers';
import {TokenResponse} from '@/models';
import {SET_USER, SET_USER_ERROR, SET_USER_LOADING, LOGOUT} from './user-types';
import {router} from '../../../router/router';
import {promiseHandler} from '../../../utils/promise';

const actions = {
    async [SET_USER]({commit}, payload) {
        commit(SET_USER_ERROR, null);
        commit(SET_USER_LOADING, true);
        const [error, token] = await promiseHandler(UserApiProvider.login(payload));
        commit(SET_USER_LOADING, false);
        if (error) {
            commit(SET_USER_ERROR, 'Invalid login credentials');
            return;
        }
        commit(SET_USER, new TokenResponse({...token}));
        router.push('/products');
    },

    logout({commit}) {
        commit(LOGOUT);
        router.push('/login');
    },
};

export default actions;
