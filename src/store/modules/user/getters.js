const getters = {
    loggedIn: (state) => !!state.data.token,
    loading: (state) => state.loading,
    getError: (state) => state.error,
    getUser: (state) => state.data,
};

export default getters;
