import actions from './actions';
import mutations from './mutations';
import getters from './getters';
import {TokenResponse} from '@/models';

const state = {
    data: new TokenResponse(),
    loading: false,
    error: null,
};

export default {
    namespaced: true,
    getters,
    state,
    actions,
    mutations,
};
