export const SET_USER = 'SET_USER';
export const SET_USER_ERROR = 'SET_USER_ERROR';
export const LOGOUT = 'LOGOUT';
export const SET_USER_LOADING = 'SET_USER_LOADING';

export const USER_LOGGED_IN = 'user/loggedIn';
