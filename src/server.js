import {Server, Model} from 'miragejs';
import {pokemonResponse} from './__mock__/pokemon-response-mock';

export const makeServer = ({environment = 'development'} = {}) => {
    return new Server({
        environment,
        models: {
            token: Model,
        },

        routes() {
            this.namespace = 'api';
            this.timing = 1000;

            this.post('/token', (_, request) => {
                const attrs = JSON.parse(request.requestBody);
                return {
                    accessToken: '0c6083f8-1ab6-408a-8398-7dac2bfd803d',
                    tokenType: 'Bearer',
                    userId: '0c6083f8-1ab6-408a-8398-7dac2bfd803d',
                    name: 'charizard',
                    email: attrs.email,
                };
            });

            this.get('/ms-ecommerce-product/products', () => {
                return pokemonResponse;
            });
            this.get('/ms-ecommerce-product/products/:id', (_, request) => {
                const id = request.params.id;
                return pokemonResponse.products.find((p) => p.id === Number(id));
            });
            this.post('/ms-ecommerce-message-producer/message/publish', (_, request) => {
                const checkout = JSON.parse(request.requestBody);
                console.log(`checkout`, checkout);
                return {
                    messageId: 'c48c5fe3-838e-440c-a20a-26005f4dd2b7',
                };
            });
        },
    });
};
