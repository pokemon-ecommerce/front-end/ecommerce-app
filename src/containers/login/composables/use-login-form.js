import {watch, computed} from 'vue';
import {useField, useForm} from 'vee-validate';
import loginFormScheme from '@/schemes/login-form-scheme';
import {useStore} from 'vuex';

const useLoginForm = () => {
    const {dispatch, getters, state} = useStore();
    const formValues = {
        email: 'test@gmail.com',
        password: '12345',
    };
    const {handleSubmit, setFieldError, errors} = useForm({
        validationSchema: loginFormScheme,
        initialValues: formValues,
    });
    const {value: email} = useField('email');
    const {value: password} = useField('password');

    const onSubmit = handleSubmit((values) => {
        dispatch('user/SET_USER', values);
    });

    watch(
        () => getters['user/getError'],
        () => setFieldError('password', getters['user/getError'])
    );
    return {
        email,
        password,
        onSubmit,
        errors,
        loading: computed(() => state.user.loading),
    };
};

export default useLoginForm;
