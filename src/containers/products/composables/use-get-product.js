import {ref} from 'vue';
import {useRoute, useRouter} from 'vue-router';
import {useStore} from 'vuex';
import {ProductApiProvider} from '@/providers';
import {toFixed} from '../../../utils/number';
const useGetProduct = () => {
    const product = ref(null);
    const loading = ref(false);
    const amount = ref(1);
    const {params} = useRoute();
    const router = useRouter();
    const {dispatch} = useStore();

    const findProduct = async () => {
        loading.value = true;
        product.value = await ProductApiProvider.findProduct(params.id);
        loading.value = false;
    };

    const getNumbers = (maxStock) => {
        const list = [];
        for (let i = 1; i <= maxStock; i++) {
            list.push(i);
        }
        return list;
    };

    const handleOnAddCart = () => {
        const {id, name, price} = product.value;
        dispatch('product/addProduct', {
            id,
            name,
            price: toFixed(price),
            amount: amount.value,
        });
        router.push('/cart');
    };

    findProduct();

    return {
        loading,
        product,
        amount,
        getNumbers,
        handleOnAddCart,
    };
};

export default useGetProduct;
