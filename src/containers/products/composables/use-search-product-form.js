import {ref} from 'vue';
import {useField, useForm} from 'vee-validate';
import {searchPoductFormScheme} from '@/schemes';
import {ProductApiProvider} from '@/providers';
const useSearchProductForm = () => {
    const productResponse = ref(null);
    const loading = ref(false);

    const searchProducts = async (filter) => {
        loading.value = true;
        productResponse.value = await ProductApiProvider.searchProducts(filter);
        loading.value = false;
    };

    const {handleSubmit, errors} = useForm({
        validationSchema: searchPoductFormScheme,
        initialValues: {
            name: '',
        },
    });
    const {value: name} = useField('name');
    const onSubmit = handleSubmit((values) => {
        searchProducts(values);
    });
    searchProducts({});
    return {
        name,
        onSubmit,
        errors,
        loading,
        productResponse,
    };
};

export default useSearchProductForm;
