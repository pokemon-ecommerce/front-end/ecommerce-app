import {ref} from 'vue';
import {useStore} from 'vuex';
import {useRouter} from 'vue-router';
import {useField, useForm} from 'vee-validate';
import {checkoutFormScheme} from '@/schemes';
import {CheckoutApiProvider} from '@/providers';
import {OrderDetails} from '@/models';

const useCheckoutForm = () => {
    const message = ref(null);
    const loading = ref(false);
    const {getters, dispatch} = useStore();
    const router = useRouter();
    const formValues = {
        cardNumber: '',
        dueDate: '',
    };
    const {handleSubmit, errors} = useForm({
        validationSchema: checkoutFormScheme,
        initialValues: formValues,
    });
    const {value: cardNumber} = useField('cardNumber');
    const {value: dueDate} = useField('dueDate');

    const checkout = async (values) => {
        loading.value = true;
        message.value = await CheckoutApiProvider.checkout(values);
        dispatch('product/purchase', message.value.messageId);
        loading.value = false;
    };

    const onSubmit = handleSubmit((values) => {
        const user = getters['user/getUser'];
        const products = getters['product/getProducts'];
        const total = getters['product/total'];
        const checkoutData = {
            payment: values,
            order: {
                userId: user.userId,
                total,
                details: products.map(
                    (item) => new OrderDetails({id: item.id, amount: item.amount})
                ),
            },
        };
        checkout(checkoutData);

        router.push('/confirmation');
    });

    return {
        cardNumber,
        dueDate,
        onSubmit,
        errors,
        loading,
    };
};

export default useCheckoutForm;
