import {ref, computed} from 'vue';
import {useStore} from 'vuex';
const useCartProducts = () => {
    const products = ref([]);
    const {getters, dispatch} = useStore();

    const loadProducts = () => {
        products.value = getters['product/getProducts'];
    };

    const removeProduct = (product) => {
        dispatch('product/removeProduct', product);
        loadProducts();
    };

    loadProducts();

    return {
        products,
        removeProduct,
        total: computed(() => getters['product/total']),
    };
};

export default useCartProducts;
