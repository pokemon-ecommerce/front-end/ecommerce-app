import ApiProvider from './api-provider';

class ProductApiProvider {
    static searchProducts = async (filter) => {
        console.log(`filter`, filter);
        return ApiProvider.get('/api/ms-ecommerce-product/products').then(
            (res) => res.data
        );
    };

    static findProduct = async (id) => {
        return ApiProvider.get(`/api/ms-ecommerce-product/products/${id}`).then(
            (res) => res.data
        );
    };
}

export default ProductApiProvider;
