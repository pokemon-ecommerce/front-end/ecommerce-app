import ApiProvider from './api-provider';

class UserApiProvider {
    static login = async (user) =>
        ApiProvider.post('/api/token', user).then((res) => res.data);
}

export default UserApiProvider;
