import axios from 'axios';

class ApiProvider {
    static instance = axios.create({
        headers: {
            'Content-Type': 'application/json',
            Accept: 'application/json',
        },
    });

    static get = (url) => this.instance.get(url);
    static post = (url, data = {}) => this.instance.post(url, data);
}

export default ApiProvider;
