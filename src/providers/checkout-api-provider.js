import ApiProvider from './api-provider';

class CheckoutApiProvider {
    static checkout = async (checkout) => {
        return ApiProvider.post(
            '/api/ms-ecommerce-message-producer/message/publish',
            checkout
        ).then((res) => res.data);
    };
}

export default CheckoutApiProvider;
