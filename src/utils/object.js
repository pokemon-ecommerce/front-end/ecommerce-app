export const isAtLeastOnePropertyIsNull = (obj = {}) => {
    return (
        Object.values(obj).filter((v) => v === null || v === 'null' || v === '').length >
        0
    );
};
