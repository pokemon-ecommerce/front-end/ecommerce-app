import {env} from '../constants/environments';

export const generateRandomNumber = (max = env.NUMBER_POKEMONS) => {
    return Math.floor(Math.random() * max) + 1;
};

export const isPairNumber = () => generateRandomNumber() % 2 === 0;

export const toFixed = (value = 0) => value.toFixed(2);
