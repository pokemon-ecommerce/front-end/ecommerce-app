export const promiseHandler = (promise) =>
    promise.then((data) => [null, data]).catch((error) => [error]);
