import {createRouter, createWebHistory} from 'vue-router';
import {USER_LOGGED_IN} from '../store/modules/user/user-types';
import store from '../store/store';

const routes = [
    {
        path: '/',
        redirect: '/products',
    },
    {
        path: '/login',
        name: '/login',
        component: () => import('../containers/login/login'),
    },
    {
        path: '/register',
        name: '/register',
        component: () => import('../containers/register/register'),
    },
    {
        path: '/products',
        name: '/products',
        component: () => import('../containers/products/products'),
    },
    {
        path: '/product/:id',
        name: '/product',
        component: () => import('../containers/products/components/product/product'),
    },
    {
        path: '/cart',
        name: '/cart',
        component: () => import('../containers/cart/cart'),
        meta: {
            requiresAuth: true,
        },
    },
    {
        path: '/checkout',
        name: '/checkout',
        component: () => import('../containers/checkout/checkout'),
        meta: {
            requiresAuth: true,
        },
    },
    {
        path: '/confirmation',
        name: '/confirmation',
        component: () => import('../containers/confirmation/confirmation'),
        meta: {
            requiresAuth: true,
        },
    },
];

const router = createRouter({
    history: createWebHistory(),
    routes,
});

router.beforeEach((to, _, next) => {
    if (to.matched.some((record) => record.meta.requiresAuth)) {
        if (store.getters[USER_LOGGED_IN]) {
            next();
            return;
        }
        next('/login');
    } else {
        next();
    }
});

export {router};
